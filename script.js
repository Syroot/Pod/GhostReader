function formatNumber(number, sign) {
	const neg = number < 0;
	number = Math.abs(number);
	const s = number ? neg ? "-" : sign ? "+" : "" : "";
	return `${s}${number}`;
}

function formatTime(time, sign) {
	const neg = time < 0;
	time = Math.abs(round(time, 3));
	const min = Math.trunc(time / 60);
	const sec = Math.trunc(time % 60);
	const mil = round(time % 1 * 1000, 0);
	const s = time ? neg ? "-" : sign ? "+" : "" : "";
	const minStr = min.toString().padStart(2, "0");
	const secStr = sec.toString().padStart(2, "0");
	const milStr = mil.toString().padStart(3, "0");
	return `${s}${minStr}\'${secStr}"${milStr}`;
}

function readFile(file) {
	return new Promise((resolve, reject) => {
		const reader = new FileReader();
		reader.onload = e => resolve(e.target.result);
		reader.onerror = e => reject(e);
		reader.readAsArrayBuffer(file);
	});
}

function round(number, decimals) {
	const x = Math.pow(10, decimals + 2);
	return (number + (1 / x)).toFixed(decimals);
}

class Ghost {
	constructor() {
		this.playerName = "";
		this.trackName = "";
		this.vehicleName = "";
		this.accel = 0;
		this.brakes = 0;
		this.grip = 0;
		this.handling = 0;
		this.speed = 0;
		this.points = 0;
		this.time = 0;
		this.startTime = 0;
		this.raceTime = 0;
		this.lapTimes = [0, 0, 0];
		this.partTimes = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
		this.partCount = 0;
		this.bestLapTime = 0;
		this.bestPartTimes = [0, 0, 0, 0];
		this.virtualLapTime = 0;
	}

	static async fromFile(file) {
		const buffer = await readFile(file);
		const read = new DataReader(buffer, true);
		let info;
		if (file.name.endsWith(".ght"))
			info = read.any(GhostFile).sequences[0].info;
		else if (file.name.endsWith(".seq"))
			info = read.any(SequenceFile).info;
		else
			throw new DataError("Unknown ghost extension.");

		const ghost = new Ghost();
		ghost.playerName = info.playerName;
		ghost.trackName = file.name;
		ghost.vehicleName = info.vehicleName;
		ghost.accel = info.accel;
		ghost.brakes = info.brakes;
		ghost.grip = info.grip;
		ghost.handling = info.handling;
		ghost.speed = info.speed;
		ghost.points = ghost.accel + ghost.brakes + ghost.grip + ghost.handling + ghost.speed;
		ghost.time = info.time;
		ghost.startTime = info.startTime;
		ghost.raceTime = info.raceTime;
		ghost.lapTimes = info.lapTimes;
		ghost.partTimes = info.partTimes;
		ghost.bestLapTime = 0;
		ghost.bestPartTimes = [0, 0, 0, 0];
		ghost.averageLapTime = 0;
		for (let l = 0; l < 3; l++) {
			const lapTime = info.lapTimes[l];
			if (!ghost.bestLapTime || lapTime < ghost.bestLapTime)
				ghost.bestLapTime = lapTime;
			for (let p = 0; p < 4; p++) {
				const partTime = info.partTimes[l][p];
				if (partTime !== 0 && (!ghost.bestPartTimes[p] || partTime < ghost.bestPartTimes[p])) {
					ghost.bestPartTimes[p] = partTime;
					if (!l)
						ghost.partCount++;
				}
			}
			ghost.averageLapTime += lapTime;
		}
		ghost.averageLapTime /= 3;
		ghost.virtualLapTime = 0;
		for (let p = 0; p < ghost.partCount; p++)
			ghost.virtualLapTime += ghost.bestPartTimes[p];
		return ghost;
	}

	static fromDiff(a, b) {
		const ghost = new Ghost();
		ghost.accel = a.accel - b.accel;
		ghost.brakes = a.brakes - b.brakes;
		ghost.grip = a.grip - b.grip;
		ghost.handling = a.handling - b.handling;
		ghost.speed = a.speed - b.speed;
		ghost.points = a.points - b.points;
		ghost.startTime = a.startTime - b.startTime;
		ghost.time = a.time - b.time;
		ghost.raceTime = a.raceTime - b.raceTime;
		ghost.partCount = Math.min(a.partCount, b.partCount);
		for (let l = 0; l < 3; l++) {
			ghost.lapTimes[l] = a.lapTimes[l] - b.lapTimes[l];
			for (let p = 0; p < ghost.partCount; p++)
				ghost.partTimes[l][p] = a.partTimes[l][p] - b.partTimes[l][p];
		}
		ghost.averageLapTime = a.averageLapTime - b.averageLapTime;
		ghost.virtualLapTime = a.virtualLapTime - b.virtualLapTime;
		return ghost;
	}
}

const COL_GHOST1 = 1;
const COL_GHOST2 = 2;
const COL_DIFF = 3;
const ROW_FILE = 0;
const ROW_PLAYER = 1;
const ROW_TRACK = 2;
const ROW_CAR = 3;
const ROW_POINTS = 4;
const ROW_ACCEL = 5;
const ROW_BRAKES = 6;
const ROW_GRIP = 7;
const ROW_HANDLING = 8;
const ROW_SPEED = 9;
const ROW_START = 10;
const ROW_AVERAGE = 11;
const ROW_VIRTUAL = 12;
const ROW_DURATION = 13;
const ROW_L1 = 14;
const ROW_L1P1 = 15;
const ROW_L1P2 = 16;
const ROW_L1P3 = 17;
const ROW_L1P4 = 18;
const ROW_L2 = 19;
const ROW_L2P1 = 20;
const ROW_L2P2 = 21;
const ROW_L2P3 = 22;
const ROW_L2P4 = 23;
const ROW_L3 = 24;
const ROW_L3P1 = 25;
const ROW_L3P2 = 26;
const ROW_L3P3 = 27;
const ROW_L3P4 = 28;
const ROW_TOTAL = 29;

const tbody = document.querySelector("tbody");
const ghosts = [];
addFileCol(0);
addFileCol(1);
addDiffCol();

function addCol() {
	const col = tbody.children[0].children.length;
	for (let i = 0; i < tbody.children.length; i++) {
		tbody.children[i].appendChild(document.createElement("td"));
	}
	return col;
}

function addDiffCol() {
	const col = addCol();
	cell(col, 0).innerHTML = "Difference";
	return col;
}

function addFileCol(index) {
	const col = addCol();
	const input = cell(col, 0).appendChild(document.createElement("input"));
	input.type = "file";
	input.onchange = async e => {
		if (!e.target.files.length)
			return;
		ghosts[index] = await Ghost.fromFile(e.target.files[0]);
		updateCol(COL_GHOST1 + index, ghosts[index]);
		if (ghosts[0] && ghosts[1])
			updateCol(COL_DIFF, Ghost.fromDiff(ghosts[0], ghosts[1]), true);
	};
	return col;
}

function cell(col, row) {
	return tbody.children[row].children[col];
}

function updateCol(col, ghost, sign) {
	cell(col, ROW_PLAYER).innerHTML = ghost.playerName;
	cell(col, ROW_TRACK).innerHTML = ghost.trackName;
	cell(col, ROW_CAR).innerHTML = ghost.vehicleName;
	cell(col, ROW_POINTS).innerHTML = formatNumber(ghost.points, sign);
	cell(col, ROW_ACCEL).innerHTML = formatNumber(ghost.accel, sign);
	cell(col, ROW_BRAKES).innerHTML = formatNumber(ghost.brakes, sign);
	cell(col, ROW_GRIP).innerHTML = formatNumber(ghost.grip, sign);
	cell(col, ROW_HANDLING).innerHTML = formatNumber(ghost.handling, sign);
	cell(col, ROW_SPEED).innerHTML = formatNumber(ghost.speed, sign);
	cell(col, ROW_START).innerHTML = formatTime(ghost.startTime, sign);
	cell(col, ROW_AVERAGE).innerHTML = formatTime(ghost.averageLapTime, sign);
	cell(col, ROW_VIRTUAL).innerHTML = formatTime(ghost.virtualLapTime, sign);
	cell(col, ROW_DURATION).innerHTML = formatTime(ghost.time, sign);
	for (let l = 0; l < 3; l++) {
		const lapStar = ghost.bestLapTime === ghost.lapTimes[l] ? "⭐" : "";
		cell(col, ROW_L1 + 5 * l).innerHTML = lapStar + formatTime(ghost.lapTimes[l], sign);
		for (let p = 0; p < 4; p++) {
			if (p < ghost.partCount) {
				const partStar = ghost.bestPartTimes[p] === ghost.partTimes[l][p] ? "⭐" : "";
				cell(col, ROW_L1P1 + 5 * l + p).innerHTML = partStar + formatTime(ghost.partTimes[l][p], sign);
			} else {
				cell(col, ROW_L1P1 + 5 * l + p).innerHTML = "";
			}
		}
	}
	cell(col, ROW_TOTAL).innerHTML = formatTime(ghost.raceTime, sign);
}
