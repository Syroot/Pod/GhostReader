const dataReaderMixin = {
	fixed() {
		return this.int32() / (1 << 16);
	},

	strbuf(length) {
		const buffer = this.bytes(length);
		return new TextDecoder().decode(buffer).replace(/\0*$/, "");
	}
};
Object.assign(DataReader.prototype, dataReaderMixin);


const dataWriterMixin = {
	fixed(value) {
		this.int32(value * (1 << 16));
	},

	strbuf(str, length) {
		const bytes = new Uint8Array(length);
		bytes.set(new TextEncoder().encode(str));
		this.bytes(bytes);
	}
};
Object.assign(DataWriter.prototype, dataWriterMixin);


class GhostFile {
	constructor() {
		this.trackIndex = 0;
		this.totalTime = 0;
		this.unknown = 0;
		this.sequences = [];
	}

	read(read) {
		if (read.int32() !== 1)
			throw new DataError("Invalid ghost version.");
		if (read.uint32() !== 0x456F1E9C)
			throw new DataError("Invalid ghost header value 1.");
		if (read.uint32() !== 0xFA48EFB4)
			throw new DataError("Invalid ghost header value 2.");

		const sequenceCount = read.int32();
		if (sequenceCount > 1)
			throw new DataError("Multiple sequences are currently not supported.");
		this.trackIndex = read.int32();
		this.totalTime = read.fixed();
		this.unknown = read.int32();

		this.sequences = [];
		for (let i = 0; i < sequenceCount; i++)
			this.sequences.push(read.any(SequenceFile));
	}
}


class SequenceFile {
	constructor() {
		this.unknown = -1;
		this.headers = [];
		this.info = new SequenceInfo();
		this.points = [];
	}

	read(read) {
		if (read.uint32() !== 0x12345678)
			throw new DataError("Invalid sequence header value 1.");
		if (read.uint32() !== 0x78451236)
			throw new DataError("Invalid sequence header value 2.");
		if (read.int32() !== 2)
			throw new DataError("Invalid sequence version.");

		const dataEnd = read.int32();
		const dataStart = read.int32();

		const headerCount = read.int32();
		this.unknown = read.int32()
		this.headers = [];
		for (let i = 0; i < headerCount; i++)
			this.headers.push(read.any(SequenceHeader));

		const dataSize = read.int32();
		this.info = read.any(SequenceInfo);

		this.points = [];
		while (!read.eof())
			this.points.push(read.any(SequencePoint));
	}
}

class SequenceHeader {
	constructor() {
		this.type = 0;
		this.data = [];
	}

	read(read) {
		this.type = read.int32();
		const size = read.int32();
		this.data = read.bytes(size);
	}
}

class SequenceInfo {
	constructor() {
		this.trackIndex = 0;
		this.time = 0;
		this.raceTime = 0;
		this.startTime = 0;
		this.lapTimes = [0, 0, 0];
		this.partTimes = [
			[0, 0, 0, 0],
			[0, 0, 0, 0],
			[0, 0, 0, 0]
		];
		this.vehicleName = "";
		this.playerName = "";
		this.grip = 0x7C;
		this.speed = 0xBD;
		this.handling = 0x3A;
		this.brakes = 0xE0;
		this.accel = 0x1E;
	}

	read(read) {
		read.offset += 4;
		this.trackIndex = read.int32();
		this.time = read.fixed();
		read.offset += 7 * 4;
		this.raceTime = read.fixed();
		this.startTime = read.fixed();
		this.lapTimes = [read.fixed(), read.fixed(), read.fixed()];
		this.partTimes = [
			[read.fixed(), read.fixed(), read.fixed(), read.fixed()],
			[read.fixed(), read.fixed(), read.fixed(), read.fixed()],
			[read.fixed(), read.fixed(), read.fixed(), read.fixed()]
		];
		read.offset += 6 * 4;
		this.vehicleName = read.strbuf(8);
		read.offset += 7 * 4;
		this.playerName = read.strbuf(8);
		read.offset += 0x32;
		this.grip = read.uint8() ^ 0x7C;
		this.speed = read.uint8() ^ 0xBD;
		this.handling = read.uint8() ^ 0x3A;
		this.brakes = read.uint8() ^ 0xE0;
		this.accel = read.uint8() ^ 0x1E;
		read.offset += 1;
		read.offset += 5 * 4;
	}
}

class SequencePoint {
	constructor() {
		this.time = 0;
		this.wheelRotation = 0;
		this.wheelSteer = 0;
		this.wheelZ = [0, 0, 0, 0];
		this.position = [0, 0, 0];
		this.rotationX = [0, 0, 0];
		this.rotationY = [0, 0, 0];
	}

	read(read) {
		this.time = read.fixed();
		this.wheelRotation = read.int32();
		this.wheelSteer = read.int32();
		this.wheelZ = [read.int32(), read.int32(), read.int32(), read.int32()];
		this.position = [read.fixed(), read.fixed(), read.fixed()];
		this.rotationX = [read.fixed(), read.fixed(), read.fixed()];
		this.rotationY = [read.fixed(), read.fixed(), read.fixed()];
	}
}
