class DataError extends Error {
	constructor(message) {
		super(message);
	}
}


class DataReader {
	constructor(buffer, littleEndian) {
		this.buffer = buffer;
		this.littleEndian = littleEndian;
		this.offset = 0;
		this.view = new DataView(buffer);
	}

	any(readable) {
		const instance = new readable();
		instance.read(this);
		return instance;
	}

	bytes(length) {
		const buffer = this.view.buffer.slice(this.offset, this.offset + length);
		this.offset += length;
		return buffer;
	}

	eof() {
		return this.offset >= this.view.byteLength;
	}

	float32() {
		const number = this.view.getFloat32(this.offset, this.littleEndian);
		this.offset += 4;
		return number;
	}

	float64() {
		const number = this.view.getFloat64(this.offset, this.littleEndian);
		this.offset += 8;
		return number;
	}

	int8() {
		const number = this.view.getInt8(this.offset, this.littleEndian);
		this.offset++;
		return number;
	}

	int16() {
		const number = this.view.getInt16(this.offset, this.littleEndian);
		this.offset += 2;
		return number;
	}

	int32() {
		const number = this.view.getInt32(this.offset, this.littleEndian);
		this.offset += 4;
		return number;
	}

	int64() {
		const number = this.view.getBigInt64(this.offset, this.littleEndian);
		this.offset += 8;
		return number;
	}

	uint8() {
		const number = this.view.getUint8(this.offset, this.littleEndian);
		this.offset++;
		return number;
	}

	uint16() {
		const number = this.view.getUint16(this.offset, this.littleEndian);
		this.offset += 2;
		return number;
	}

	uint32() {
		const number = this.view.getUint32(this.offset, this.littleEndian);
		this.offset += 4;
		return number;
	}

	uint64() {
		const number = this.view.getBigUint64(this.offset, this.littleEndian);
		this.offset += 8;
		return number;
	}
}


class DataWriter {
	constructor(buffer, littleEndian) {
		this.buffer = buffer;
		this.littleEndian = littleEndian;
		this.offset = 0;
		this.view = new DataView(buffer);
	}

	any(writable) {
		writable.write(this);
	}

	bytes(buffer) {
		new Uint8Array(this.view.buffer, this.offset, buffer.byteLength).set(buffer);
		this.offset += buffer.byteLength;
	}

	eof() {
		return this.offset >= this.view.byteLength;
	}

	float32(number) {
		this.view.setFloat32(this.offset, number, this.littleEndian);
		this.offset += 4;
	}

	float64(number) {
		this.view.setFloat64(this.offset, number, this.littleEndian);
		this.offset += 8;
	}

	int8(number) {
		this.view.setInt8(this.offset, number, this.littleEndian);
		this.offset++;
	}

	int16(number) {
		this.view.setInt16(this.offset, number, this.littleEndian);
		this.offset += 2;
	}

	int32(number) {
		this.view.setInt32(this.offset, number, this.littleEndian);
		this.offset += 4;
	}

	int64(number) {
		this.view.setBigInt64(this.offset, number, this.littleEndian);
		this.offset += 8;
	}

	uint8(number) {
		this.view.setUint8(this.offset, number, this.littleEndian);
		this.offset++;
	}

	uint16(number) {
		this.view.setUint16(this.offset, number, this.littleEndian);
		this.offset += 2;
	}

	uint32(number) {
		this.view.setUint32(this.offset, number, this.littleEndian);
		this.offset += 4;
	}

	uint64(number) {
		this.view.setBigUint64(this.offset, number, this.littleEndian);
		this.offset += 8;
	}
}
